# Instalação e configuração

  
  

## Banco de Dados

A tecnologia utilizada foi o MySQL, o script de criação pode ser encontrado dentro da raiz do projeto na pasta db

  

## Server

### Instalação

Em raiz/server executar o comando para instalar as dependencias do npm:

    npm install

  

### Configurações

dentro da pasta do servidor existe um arquivo .env em que as principais variaveis já estão pré configuradas para uso

  

### Execução

o arquivo que executa o servidor esta localizado em: raiz/server/src/index.js. Seja com o próprio node, com o pm2 ou nodemon por exemplo

  
  

## Client

### Instalação

Em raiz/server executar o comando para instalar as dependencias do npm:

    npm install

  

### Configurações

dentro da pasta de cliente as variaveis de dependencia estao armazenadas dentro de:

    raiz/config/prod.env.js

  

### Execução

Dentro de raiz/client executar: `npm run dev` para desenvolvimento, ou então `npm run prod` para produção (gerando desta maneira os arquivos prontos na pasta dist)