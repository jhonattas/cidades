import restify from 'restify'
const server 	      = restify.createServer()
const cors 		      = require('./cors')
const routes 	      = require('../http/routes')

// app
import favoritesController  from'../controllers/favoritesController'
import clientController     from'../controllers/clientController'

server.pre(cors.preflight)
server.use(cors.actual)
server.use(restify.plugins.bodyParser())

routes(server)

favoritesController(server)
clientController(server)

server.use(
  function crossOrigin(req,res,next){
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "X-Requested-With")
    return next()
  }
)

module.exports = server
