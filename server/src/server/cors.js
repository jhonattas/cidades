const corsMiddleware = require('restify-cors-middleware')

const cors = corsMiddleware({
	preflightMaxAge: 5,
	origins: [
		'http://localhost:8080'
	],
	allowHeaders: ['Content-Type', 'Access-Control-Allow-Headers', 'Access-Control-Allow-Origin', 'x-access-token', 'authorization'],
	exposeHeaders: ['*'],
	credentials: true
})

module.exports = cors
