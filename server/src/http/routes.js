const db = require('../services/mysql');
const restify = require('restify')

const routes = (server) => {
    server.get('/', (req, res, next) => {
        res.send('Teste Siteware');
        next();
    });

    server.get(/\/static\/?.*/, restify.plugins.serveStatic({
        directory: __dirname
    }));

}

module.exports = routes
