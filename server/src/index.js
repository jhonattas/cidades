require('dotenv').config();
require("babel-core/register");
require("babel-polyfill");
const server = require('./server');

server.listen(process.env.PORT || 5000, function() {
  console.log('server listening ' + process.env.PORT || 5000);
});
