import connection from './connection'

const errorHandler = (error, msg, rejectFunction) => {
  console.error(error);
  rejectFunction({ error: msg })
};

const favoritesModule   = require('./dbFavorites')  ({ connection, errorHandler })

function favorites() { return favoritesModule; }

export { favorites }
