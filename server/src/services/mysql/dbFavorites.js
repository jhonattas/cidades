import connection from './connection'
const table_name = 'favorites'

const favorites = deps => {
  return {
    byUUIDNoFilter: (uuid) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps

        var queryString = `SELECT * FROM ${table_name} where client_uuid = (?)`
        var queryData = [uuid]

        connection.query(queryString, queryData, (error, results) => {
          if(error) {
            errorHandler(error, 'Não foi possivel listar favoritos do cliente', reject)
            return false
          }
          resolve({ cities: results })
        })
      })
    },

    byUUID: (name) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps

        var queryString = `SELECT * FROM ${table_name} where client_uuid = (?) and active = 1`
        var queryData = [name]

        connection.query(queryString, queryData, (error, results) => {
          if(error) {
            errorHandler(error, 'Falha ao retornar favoritos', reject)
            return false
          }
          resolve({ cities: results })
        })
      })
    },

    checkFavorite: (uuid, city_id) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps

        var queryString = `SELECT count(*) FROM ${table_name} where client_uuid = ? and city_id = ?`
        var queryData = [uuid, city_id]

        connection.query(queryString, queryData, (error, results) => {
          if(error) {
            errorHandler(error, 'A cidade não foi favoritada ainda', reject)
            return false
          }

          resolve(results[0]['count(*)'])
        })
      })
    },

    save: (client_uuid, city_id, name, country) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        var active = 1
        var query = `INSERT INTO ${table_name} (client_uuid, city_id, name, country, active, created_at) VALUES (?, ?, ?, ?, ?, NOW())`
        var queryData = [client_uuid, city_id, name, country, active]

        connection.query(query, queryData, (error, results) => {
          if(error) {
            errorHandler(error, `Falha ao inserir favorito ${city_id}. para o cliente: ${client_uuid}`, reject)
            console.log(error)
            return false
          }
          resolve({ city: { client_uuid, city_id, country, name, active } })
        })
      })
    },

    disable: (id) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        var queryString = `DELETE FROM ${table_name} WHERE id = (?)`
        var queryData = [id]
        connection.query(queryString, queryData, (error, results) => {
          if(error || !results.affectedRows) {
            errorHandler(error, `Falha ao remover a favorito ${id}`, reject)
            return false
          }
          resolve({ favorite: { id }, affectedRows: results.affectedRows })
        })
      })
    },
  }
}

module.exports = favorites
