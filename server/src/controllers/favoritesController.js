import { favorites } from '../services/mysql';

const favoritesController = (server) => {

  // retorna os TODOS os favoritos de um cliente baseado no uuid do mesmo
  server.get('favorites/:uuid/all', async (req, res, next) => {
    const { uuid } = req.params
    try {
      res.send(await favorites().byUUIDNoFilter(uuid))
    } catch (error){
      console.log(error)
      res.send(error)
    } finally {
      next()
    }
  })

  // retorna os favoritos ativos de um cliente
  server.get('favorites/:uuid/active', async (req, res, next) => {
    const { uuid } = req.params
    try {
      res.send(await favorites().byUUID(uuid))
    } catch (error){
      console.log(error)
      res.send(error)
    } finally {
      next()
    }
  })

  // salva uma cidade favorita de um cliente
  server.post('favorite', async (req, res, next) => {
    const { uuid, cityId, name, country } = req.body
    try {
      favorites().checkFavorite(uuid, cityId).then(function (result) {

        if(result === 0) {
          favorites().save(uuid, cityId, name, country)
          res.send(200, {
            "status": "success" })
        } else {
          res.json(500, {
            "status": "error",
            "description": "a cidade ja foi previamente favoritada para este cliente"
           });
        }
      })

    } catch (error){
      console.log(error)
      res.send(error)
    } finally {
      next()
    }
  })

  // remove um favorito de um clientw
  server.del('favorite/:id', async (req, res, next) => {
    const { id } = req.params

    try {
      res.send(await favorites().disable(id))
    } catch (error){
      res.send(error)
    } finally {
      next()
    }
  })
}

export default favoritesController
