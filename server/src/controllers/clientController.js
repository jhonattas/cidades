const clientController = (server) => {

  // retorna os TODOS os favoritos de um cliente baseado no uuid do mesmo
  server.get('client/new', async (req, res, next) => {
    try {
      var count = 35
      var _sym = process.env.UUID_SECRET
      var str = ''

      for(var i = 0; i < count; i++) {
        str += _sym[parseInt(Math.random() * (_sym.length))];
      }
      res.json({ "uuid": str });
    } catch (error){
      res.send(error)
    } finally {
      next()
    }
  })
}

export default clientController
