import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/pages/Welcome'
import Favorites from '@/pages/Favorites'
import CityDetails from '@/pages/CityDetails'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: Welcome
    },
    {
      path: '/favorites',
      name: 'client_favorites',
      component: Favorites
    },
    {
      path: '/city/:id',
      name: 'city_details',
      component: CityDetails
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
  mode: 'history'
})
